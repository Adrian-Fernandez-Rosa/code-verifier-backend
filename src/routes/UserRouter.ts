//crear una ruta que va a utilizar una instancia de helloController, para hacer llamadas 
//para poder hacer llamadas a sus métodos y recibir mensajes 

import express,{ Request, Response  } from "express";
import { UserController } from "../controller/UsersController";
import { LogInfo } from "../utils/logger";

// Router from express 
let usersRouter = express.Router();

//creando ruta GET -> http://localhost:8000/api/users?id=625b65534b8af64995689aef
//no tendra body, intentaremos leer parametros
usersRouter.route('/')
     // GET: 
    .get(async (req: Request, res: Response) => {
      
        // Obtain a Query Param
        let id: any = req?.query?.id;
        LogInfo(`Query param: ${id}`);

        // Controller Instance to execute method
        const controller: UserController= new UserController(); 
        // <-- comunicandose con controller(helloController).
        // Obtain Response (consumir como promesa)
        const response: any = await controller.getUsers(id); //async
        // Send to the client the response
        return res.send(response)

    })
    .delete(async (req:Request, res: Response) => {
        // Obtain a Query Param(ID)
        let id: any = req?.query?.id;
        LogInfo(`Query Param: ${id}`);

         // Controller Instance to execute method
         const controller: UserController= new UserController(); 
         // Obtain Response (consumir como promesa)
         const response: any = await controller.getUsers(id); //async
         // Send to the client the response
         return res.send(response)


    })
    // .post
    .post (async (req:Request, res: Response) => {

        
        let name: any = req?.query?.name;
        let age: any = req?.query?.age;
        let email: any = req?.query?.email;

        const controller: UserController= new UserController(); 

        let user = {
            name: name || 'default',
            email: email || 'default email',
            age: age || 18
        }

        // Obtain Response (consumir como promesa)
        const response: any = await controller.createUser(user); //async
        // Send to the client the response
        return res.send(response)

    })
    .put(async (req:Request, res: Response) => {
            // Obtain a Query Param (ID)
            let id: any = req?.query?.id; 
            let name: any = req?.query?.name;
            let age: any = req?.query?.age;
            let email: any = req?.query?.email;

            LogInfo(`Query Param: ${id}, ${name}, ${age}, ${email}`);

            const controller: UserController= new UserController(); 

            let user = {
                name: name ,
                email: email ,
                age: age 
            }

             // Obtain Response (consumir como promesa)
        const response: any = await controller.updateUser(id, user); 

        return res.send(response);
    })
   
    // Export Hello Router
    export default usersRouter;
    //ahora se añadira la ruta al rootrouter