//crear una ruta que va a utilizar una instancia de helloController, para hacer llamadas 
//para poder hacer llamadas a sus métodos y recibir mensajes 

import { BasicResponse } from "@/controller/types";
import express,{ Request, Response  } from "express";
import { HelloController } from "../controller/HelloController";
import { LogInfo } from "../utils/logger";

// Router from express 
let helloRouter = express.Router();

//creando ruta GET -> http://localhost:8000/api/hello?name=Martin/ 
//no tendra body, intentaremos leer parametros
helloRouter.route('/')
     // GET: 
    .get(async (req: Request, res: Response) => {
        // Obtain a Query param
        let name: any = req?.query?.name;
        LogInfo(`Query param: ${name}`);
        // Controller Instance to execute method
        const controller: HelloController= new HelloController(); 
        // <-- comunicandose con controller(helloController).
        // Obtain Response (consumir como promesa)
        const response: BasicResponse = await controller.getMessage(name); //async
        // Send to the client the response
        return res.send(response)

    })
    // .post

    // Export Hello Router
    export default helloRouter;