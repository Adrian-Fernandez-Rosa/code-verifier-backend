/**
 * Root Router
 * Redirections to Router
 */
import express, { Request, Response} from 'express';
import helloRouter from './HelloRouter';
import { LogInfo } from '../utils/logger';
import GoodbyeRouter from './GoodbyeRouter';
import usersRouter from './UserRouter';

// Server instance
let server = express(); 
//nos permitira luego definir que rutas se redireccionara hacia un lado o el otro.

// Router instance
let rootRouter = express.Router();

// Activate for requests to http://localhost:8000/api

rootRouter.get("/", (req: Request, res: Response) => {
    LogInfo('Get: http://localhost:8000/api')
    // Send Hello World
    res.send('Welcome to my API Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose')
});

// Redirections to Routers & Controllers
// Que la raiz sea gestionada por rootRouter:
server.use('/', rootRouter); //http://localhost:8000/api
server.use('/hello', helloRouter); // http://localhost:8000/api/hello --> HelloRouter
server.use('/goodbye', GoodbyeRouter)// http://localhost:8000/api/goodbye --> GoodbyeRouter
server.use('/users', usersRouter);// http://localhost:8000/api/users --> UsersRouter

// Entonces todas las rutas a nivel de raiz se gestionara con rootRouter
// Mientras las que vaya /hello por helloRouter

// Add more routes to the app

export default server;