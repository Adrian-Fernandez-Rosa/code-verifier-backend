import express, { Express, Request, Response } from 'express';

// Swagger
import swaggerUi from 'swagger-ui-express'; 


// Security
import cors from 'cors';
import helmet from 'helmet';

// TODO: HTTPS

// Root Router
import rootRouter from '../routes'; //por defecto ya importa el index, no hace falta especificar
import mongoose from 'mongoose';

// Create Express APP
const server: Express = express();


// * Swagger Config and route
server.use(
    '/docs',
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
        swaggerOptions: { //donde esta el contenido que tiene que mostrar
            url: "/swagger.json",
            explorer: true //nos permitira hacer búsquedas como diferentes versiones de api , etc
        }
    })
);


// Define SERVER to use "/api" and use rootRouter from 'index.ts' in routes
// From this point onover: http://localhost:8000/api/...
server.use(
    '/api',
    rootRouter
    );

    // Static Server 
    server.use(express.static('public'));
    

    // TODO: Mongoose Connection
    mongoose.connect('mongodb://localhost:27017/codeVerification')

// Security Config
server.use(helmet());
server.use(cors());

// Content Type Config.
server.use(express.urlencoded({ extended: true, limit: '50mb'}));//extended propiedad extra
server.use(express.json({limit: '50mb'}));//peticiones json
//limite de megas 

//  Redirection Config
// http://localhost:8000/ --> http://localhost/8000/api/
server.get("/", (req: Request, res: Response) => {
    res.redirect('/api');
});

export default server;