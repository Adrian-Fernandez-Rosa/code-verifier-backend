import { userEntity } from "../entities/User.entity";

import { LogSuccess, LogError } from "../../utils/logger";

// CRUD

/**
 * Method to obtain all Users from Collection "Users" in Mongo Server
 */
export const getAllUsers = async (): Promise<any[] | undefined> => {

    try {
        let userModel = userEntity();

      // console.log(userModel.find({isDelete: false}))
        // search all users
        return await userModel.find({isDelete: false})
        
    } catch (error){
        LogError(`[ORM ERROR]: Getting All Users: ${error}`);
    }
}
// - Get User by ID
export const getUserByID = async (id: string) : Promise<any | undefined> => {

    try {
        let userModel = userEntity();

        //search User By ID
        return await userModel.findById(id);


    } catch (error) {
        LogError(`[ORM ERROR]: Getting  User By ID: ${error}`);

    }

}
// - Delete User By ID
export const deleteUserByID = async (id: string) : Promise<any | undefined> => {
    
    try {
        let userModel = userEntity();

        return await userModel.deleteOne({ _id: id})


    } catch (error) {
        LogError(`[ORM ERROR]: Deleting  User By ID: ${error}`);

    }
}

// - Create New User
export const createUser = async (user: any): Promise<any | undefined> => {

    try {

        let userModel = userEntity();

        // Create / Insert new User

        return await userModel.create(user);
        

        
    } catch (error) {
        LogError(`[ORM ERROR]: Creating User: ${error}`);
    }

}

// - Update User By ID
export const updateUserByID = async (id: string, user: any): Promise<any | undefined> => {

    try {
        let userModel = userEntity();

        // Update User
        return await userModel.findByIdAndUpdate(id, user); 
        //<- buscara por id y sobreescribira el user


    } catch (error) {
        LogError(`[ORM ERROR]: Updating user ${id}: ${error}`)
    }
}


// TODO:

// - Get User by Email






