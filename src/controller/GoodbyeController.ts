import { DateResponse } from "./types";
import { LogSuccess } from "../utils/logger";

export class GoodbyeController {

    public async getMessage(name?: string): Promise<DateResponse> {
        LogSuccess("[api/goodbye] Get Request");

        return {
            message: "pruebaaa",
            Date: new Date()
        }
    }

}