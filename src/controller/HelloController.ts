import { Get, Query, Route, Tags } from "tsoa"; //solo por Get
import { BasicResponse } from "./types";
import { IHelloController } from "./interfaces";
import { LogSuccess } from "../utils/logger"; //del utils logger

@Route("/api/hello")
@Tags("HelloController")
export class HelloController implements IHelloController{
   
    /**
     * Endpoint to retreive a Message "Hello {name}" in JSON.
     * @param {string | undefined } name Name of user to be greeted 
     * @returns {BasicResponse} Promise of Basicresponse
     */
    @Get("/") //documentando que es get
    public async getMessage(@Query()name?: string): Promise<BasicResponse> {
        //<- indicamos que el name es tipo query
        LogSuccess("[api/hello] Get Request");

        return {
            message: `Hello ${name || "world"}` //En caso de que sea nulo pondria world
        }
        
    }

} //clase que gestionara rutas