import { BasicResponse } from "../types";


export interface IHelloController {
    getMessage(name?:string): Promise<BasicResponse> //basic responde de nuestra otra clase
    //cuando implementemos en el controlador debe tener una funcion getMessage que
    //va a tener un nombre opcional de tipo string y este va a devolver una promesa que va a ser de tipo
    //BasicResponse
}//cuando deseemos agregar mas funcionalidades al controlador simplemente, las declaramos aqui

export interface IUserController {

    // Read all users from database || get User By ID
    getUsers(id?: string): Promise<any>
   
    // Delete User By ID
     deleteUser(id?:string): Promise<any>
     
     // Create new User
     createUser(user : any): Promise<any>
     // Update user
     updateUser(id:string, user: any): Promise<any>

}