//va exportar los diferentes tipos de respuestas que va devolver el controlador.
/**
 * Basic json response for controllers.
 */
export type BasicResponse = { //son como las enum en java
    message: string //json de respuesta
} 

/**
 * Error JSON response for Controllers.
 */
export type ErrorResponse = {
    error: string,
    message: string
}

export type DateResponse = {
    message: string,
    Date: Date
}